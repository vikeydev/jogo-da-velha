import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    return (
        <button className="square" onClick={ props.onClick }>
            { props.value }
        </button>
    );
}
  
class Board extends React.Component {
    renderSquare(i) {
        return <Square value={ this.props.squares[i] } onClick={ () => this.props.onClick(i) } />;
    }
  
    render() {
        return (
            <div>
                <div className="board-row">
                    { this.renderSquare(0) }
                    { this.renderSquare(1) }
                    { this.renderSquare(2) }
                </div>
                <div className="board-row">
                    { this.renderSquare(3) }
                    { this.renderSquare(4) }
                    { this.renderSquare(5) }
                </div>
                <div className="board-row">
                    { this.renderSquare(6) }
                    { this.renderSquare(7) }
                    { this.renderSquare(8) }
                </div>
            </div>
        );
    }
}
  
class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
            }],
            xIsNext: true,
            stepNumber: 0
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        var previousPlayer = this.state.xIsNext ? 'O' : 'X';
        var hasWinner = calculateWinner(squares);
        var gameOver = calculateGameOver(squares, previousPlayer);
        if (squares[i] || hasWinner || gameOver) { // itself or winner or gameOver
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
            }]),
            xIsNext: !this.state.xIsNext,
            stepNumber: history.length
        });
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        
        const winner = calculateWinner(current.squares);
        var previousPlayer = this.state.xIsNext ? 'O' : 'X';
        const gameOver = calculateGameOver(current.squares, previousPlayer);

        const moves = history.map((step, move) => {
            const desc = move ? 'Jogada #' + move : 'Reiniciar';
            return (
                <li key={ move }>
                    <button onClick={ () => this.jumpTo(move) }>{ desc }</button>
                </li>
            );
        });
        
        var nextPlayer = this.state.xIsNext ? 'X' : 'O';
        let status;
        if (winner) {
            status = 'O Ganhador foi ' + previousPlayer + '. Parabéns!';
            for (let j = 0; j < winner.length; j++) {
                current.squares[winner[j]] = 'G';
            }
        } else if (gameOver) {
            status = 'Acabou! Ninguém ganhou.';
        } else {
            status = 'Prox. jogador: ' + nextPlayer;
        }

        return (
            <div className="game">
                <div className="game-info">
                    <ol>{ moves }</ol>
                </div>
                <div className="game-board">
                    <Board squares={ current.squares } onClick={ (i) => this.handleClick(i) }/>
                    <div><br/>{ status }</div>
                </div>
            </div>
        );
    }
}

function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return [a, b, c];
        }
    }
    return false;
}

function calculateGameOver(squares, previousPlayer) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    var linesAvailable = 8;
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] === previousPlayer || squares[b] === previousPlayer || squares[c] === previousPlayer) {
            linesAvailable--;
        }
    }
    if(linesAvailable <= 0) {
        return true;
    } else {
        return false;
    }
}

ReactDOM.render(<Game />, document.getElementById('root'));
  